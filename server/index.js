const express = require('express')
const devServer = require('./development')
const prodServer = require('./production')

const DEFAULT_DEV_PORT = 9001
const devMode = process.env.NODE_ENV === 'development'
const server = devMode ? devServer : prodServer
const app = server(express())

const listener = app.listen(DEFAULT_DEV_PORT, () => {
  console.log(`listening @ http://localhost:${listener.address().port}/`)
})

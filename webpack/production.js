const path = require('path');
const nodeExternals = require('webpack-node-externals');
const webpack = require('webpack');
const merge = require('webpack-merge');
const StatsPlugin = require('stats-webpack-plugin');
const rootPath = require('../root-path');
const common = require('./common');

console.log('prod config')

module.exports = [
  merge(common, {
    name: 'client',
    mode: 'production',
    target: 'web',
    entry: {
      client: [
        '@babel/polyfill',
        './src/client',
      ],
      vendor: [
        '@babel/polyfill',
        'react',
        'react-dom',
        'react-helmet',
        'react-router',
        'react-router-config',
        'react-router-dom'
      ],
    },
    output: {
      path: path.resolve(rootPath, 'dist'),
      filename: '[name].bundle.js',
      publicPath: '/'
    },
  }),
  merge(common, {
    name: 'server',
    mode: 'production',
    target: 'node',
    externals: [nodeExternals()],
    entry: './src/server',
    output: {
      path: path.resolve(rootPath, 'dist'),
      filename: 'server.js',
      publicPath: '/',
      libraryTarget: 'commonjs2'
    },
    plugins: [
			new StatsPlugin('stats.json', {
				chunkModules: true,
				modules: true,
				chunks: true,
				exclude: [/node_modules[\\\/]react/],
			}),
    ]
  })
];
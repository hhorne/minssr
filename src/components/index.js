import React from 'react'
import { Link, Route } from 'react-router-dom'

export { default as Root } from './root'

export const Home = () => <div>home</div>

export const Test = () => <div>test</div>

export const NotFound = () => 
  <Route render={({ staticContext }) => {
    if (staticContext) {
      staticContext.status = 404;
    }

    return (
      <div className="not-found">
        <h1>404 : Not Found </h1>
      </div>
    )
  }} />
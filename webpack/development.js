const path = require('path');
const nodeExternals = require('webpack-node-externals');
const webpack = require('webpack');
const merge = require('webpack-merge');
const rootPath = require('../root-path');
const common = require('./common')

console.log('dev config')

module.exports = [
  merge(common, {
    name: 'client',
    target: 'web',
    mode: 'development',
    entry: {
      client: [
        '@babel/polyfill',
        'webpack-hot-middleware/client',
        './src/client',
      ],
      vendor: [
        '@babel/polyfill',
        'react',
        'react-dom',
        'react-helmet',
        'react-router',
        'react-router-config',
        'react-router-dom'
      ]
    },
    output: {
      path: path.resolve(rootPath, 'dist'),
      filename: '[name].bundle.js',
      publicPath: '/'
    },
    plugins: [
      new webpack.HotModuleReplacementPlugin(),
      new webpack.NamedModulesPlugin(),
    ],
  }),
  merge(common, {
    name: 'server',
    target: 'node',
    mode: 'development',
    externals: [nodeExternals()],
    entry: './src/server',
    output: {
      path: path.resolve(rootPath, 'dist'),
      filename: 'server.js',
      publicPath: '/',
      libraryTarget: 'commonjs2'
    },
    plugins: [
      new webpack.NamedModulesPlugin(),
    ]
  })
];
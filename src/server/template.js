export default ({ helmet, markup }) => {
  return `
    <!doctype html>
    <html ${helmet.htmlAttributes.toString()}>
    <head>
      ${helmet.title.toString()}
      ${helmet.meta.toString()}
      ${helmet.link.toString()}
    </head>
    <body ${helmet.bodyAttributes.toString()}>
      <div id="app">${markup}</div>
      <script src="/vendor.bundle.js"></script>
      <script src="/runtime~client.bundle.js"></script>
      <script src="/client.bundle.js"></script>
    </body>
    </html>
  `
}
const webpack = require('webpack')
const webpackDevMiddleware = require('webpack-dev-middleware')
const webpackHotMiddleware = require('webpack-hot-middleware')
const webpackHotServerMiddleware = require('webpack-hot-server-middleware')

const findClient = c => c.name === 'client'

module.exports = function developmentServer(app) {
  console.log('dev server')
  const config = require('../webpack/development')
  const compiler = webpack(config)

  app.use(webpackDevMiddleware(compiler, {
    publicPath: '/',
    stats: true,
    serverSideRender: true,
  }))
  app.use(webpackHotMiddleware(compiler.compilers.find(findClient)))
  app.use(webpackHotServerMiddleware(compiler))

  return app
}
module.exports = {
  devtool: 'cheap-module-eval-source-map',
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: 'style-loader',
          },
          {
            loader: 'css-loader',
            options: {
              modules: true,
              importLoaders: 1,
              localIdentName: '[name]__[local]___[hash:base64:5]',
              sourceMap: true
            }
          },
        ]
      },
      {
        test: /\.(png|jpg|gif|eot|svg|ttf|woff(2)?)$/,
        use: [{ 
          loader: 'url-loader',
          options: {
            name: 'static/[name].[ext]'
          }
        }]
      },
    ]
  },
}
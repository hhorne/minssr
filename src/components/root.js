import React from 'react'
import { Helmet } from 'react-helmet'
import { renderRoutes } from 'react-router-config'
import routes from '../routes'

import 'semantic-ui-css/semantic.min.css'

export default ({ route }) => (
  <div>
    <Helmet
      htmlAttributes={{lang: 'en', amp: undefined}} // amp takes no value
      titleTemplate='%s'
      titleAttributes={{itemprop: 'name', lang: 'en'}}
      meta={[
        {name: 'description', content: 'draftbored'},
        {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      ]}
    />
    <div>
      {renderRoutes(route.routes)}
    </div>
  </div>
)
import React from 'react'
import { hydrate } from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import { renderRoutes } from 'react-router-config'
import routes from './routes'

const container = document.getElementById('app')
const compositionRoot = (
  <BrowserRouter>
    {renderRoutes(routes)}
  </BrowserRouter>
)

hydrate(compositionRoot, container)

if (module.hot) {
  module.hot.accept()
}
import { Home, NotFound, Root, Test } from './components'

export default [
  {
    component: Root,
    routes: [
      {
        path: '/',
        exact: true,
        component: Home,
      },
      {
        path: '/home',
        exact: true,
        component: Home,
      },
      {
        path: '/test',
        component: Test
      },
      {
        path: '*',
        component: NotFound,
      }
    ]
  }
]
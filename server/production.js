const express = require('express')
const compression = require('compression')
const path = require('path')
const rootPath = require('../root-path')

module.exports = function productionServer(app) {
  console.log('prod server')
  const distPath = path.join(rootPath, './dist')
  const ServerRendererPath = path.join(distPath, './server.js')
  const ServerRenderer = require(ServerRendererPath).default
  const ClientStatsPath = path.join(distPath, './stats.json')
  const Stats = require(ClientStatsPath)

  app.use(compression())
  app.use(express.static(distPath))
  app.use(ServerRenderer(Stats))

  return app
}
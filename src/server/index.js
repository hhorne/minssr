import React from 'react'
import { renderToString } from 'react-dom/server'
import { StaticRouter } from 'react-router'
import { renderRoutes } from 'react-router-config'
import { Helmet } from 'react-helmet'
import Template from './template'
import routes from '../routes'

const HTTP_OK = 200

export default function renderer({ clientStats, serverStats }) {
  return (request, response, next) => {
    let context = { status: HTTP_OK }
    const helmet = Helmet.renderStatic()
    const markup = renderToString(
      <StaticRouter location={request.url} context={context}>
        {renderRoutes(routes)}
      </StaticRouter>
    )

    response
      .status(context.status)
      .send(
        Template({
          helmet,
          markup,
        })
      )
  }
}